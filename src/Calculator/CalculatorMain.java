package Calculator;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

public class CalculatorMain {

	public static CalculatorMain calculator;
	protected JFrame frame;
	protected JLabel answer;
	protected JLabel answerRecord;
	protected JLabel recordLabel;
	//왜 라벨이 3개인가..
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {	
					calculator = new InputedNumberController();
					//번호누른것을 담는다??
					calculator.frame.setVisible(true);		//설정하다 표시하기 		
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	//생성자  와 메인 컴퓨터가 진행하는 순서?? =============
	/**
	 * @wbp.parser.entryPoint
	 */
	public CalculatorMain() {
		//이름이 왜 필요하지??? ================
		JLabel answer = new JLabel("");
		answer.setName("asdasdsa");
		
		answerRecord = new JLabel("");
		recordLabel =new JLabel("");
		initialize();
		
	}

	private void initialize() {
		frame = new JFrame();
		frame.setTitle("calculator");
		frame.getContentPane().setBackground(new Color(188, 188, 188));
		frame.setBackground(new Color(0, 0, 0));
		frame.setSize(446, 700); //      실행할때 쓰는 창크기 다 
		
		
		
		
		
		JLabel enSharpLabel = new JLabel("EN#");
		enSharpLabel.setBackground(new Color(0, 191, 255));
		enSharpLabel.setForeground(new Color(135, 206, 235));
		enSharpLabel.setBounds(23, 49, 293, 119);
		frame.getContentPane().add(enSharpLabel);
		//라벨 생성 
		
		
		
		answerRecord.setHorizontalAlignment(SwingConstants.RIGHT);//수평 정렬 
		answerRecord.setBounds(18, 58, 399, 40);
		answerRecord.setBorder(new BevelBorder(BevelBorder.LOWERED));//여백???    안으로 들어간 모양  밖으로 돌출모양 RAISED
		frame.getContentPane().add(answerRecord);
		//라벨 화면에 띄우기 
		
		
		recordLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		recordLabel.setBounds(18, 18, 399, 40);
		recordLabel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		frame.getContentPane().add(recordLabel);
		
		
		JButton buttonNumberAdd = new JButton(".");
		buttonNumberAdd.setForeground(new Color(0, 0, 0));
		buttonNumberAdd.setBackground(new Color(135, 206, 235));
		buttonNumberAdd.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Number(".");
			}
		});
		
		buttonNumberAdd.setBounds(225, 527, 80, 80);
		frame.getContentPane().add(buttonNumberAdd);
		
		
		JButton buttonNumber0 = new JButton("0");
		buttonNumber0.setForeground(new Color(0, 0, 0));
		buttonNumber0.setBackground(new Color(135, 206, 235));
		buttonNumber0.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Number("0");
			}
		});
		buttonNumber0.setBounds(123, 527, 80, 80);
		frame.getContentPane().add(buttonNumber0);

	
		//버튼안에 함수를 넣었내 오,,,,
		JButton buttonNumber1 = new JButton("1");
		buttonNumber1.setForeground(new Color(0, 0, 0));
		buttonNumber1.setBackground(new Color(135, 206, 235));
		buttonNumber1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Number("1");
			}
		});		
		buttonNumber1.setBounds(19, 427, 80, 80);// 버튼 위치를 말함     
		frame.getContentPane().add(buttonNumber1); //  return getRootPane().getContentPane(); 이거 뭐야  각각 함수 안에 값이 있는지 없는지 판단한다 
		
		
		JButton buttonNumber2 = new JButton( "2");
		buttonNumber2.setBackground(new Color(0, 0, 0));
		buttonNumber2.setBackground(new Color(135, 206, 235));
		buttonNumber2.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Number("2");
			}
		});
		buttonNumber2.setBounds(124, 427, 80, 80);
		frame.getContentPane().add(buttonNumber2);

		
		
		JButton buttonNumber3 = new JButton("3");
		buttonNumber3.setForeground(new Color(0, 0, 0));
		buttonNumber3.setBackground(new Color(135, 206, 235));
		buttonNumber3.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Number("3");
			}
		});
		buttonNumber3.setBounds(226, 427, 80, 80);
		frame.getContentPane().add(buttonNumber3);
		
		
		
		JButton buttonNumber4 = new JButton("4");
		buttonNumber4.setForeground(new Color(0, 0, 0));
		buttonNumber4.setBackground(new Color(135, 206, 235));
		buttonNumber4.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Number("4");
			}
		});
		buttonNumber4.setBounds(19, 315, 80, 80);
		frame.getContentPane().add(buttonNumber4);


		
		JButton buttonNumber5 = new JButton("5");
		buttonNumber5.setBackground(new Color(0, 0, 0));
		buttonNumber5.setBackground(new Color(135, 206, 235));
		buttonNumber5.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Number("5");
			}
		});
		buttonNumber5.setBounds(124, 315, 80, 80);
		frame.getContentPane().add(buttonNumber5);


		JButton buttonNumber6 = new JButton("6");
		buttonNumber6.setForeground(new Color(0, 0, 0));
		buttonNumber6.setBackground(new Color(135, 206, 235));
		buttonNumber6.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Number("6");
			}
		});
		buttonNumber6.setBounds(226, 315, 80, 80);
		frame.getContentPane().add(buttonNumber6);
		
		//¼ýÀÚ7¹öÆ°
		JButton buttonNumber7 = new JButton("7");
		buttonNumber7.setForeground(new Color(0, 0, 0));
		buttonNumber7.setBackground(new Color(135, 206, 235));
		buttonNumber7.setBounds(19, 204, 80, 80);
		buttonNumber7.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Number("7");
			}
		});
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(buttonNumber7);
		
		
		//¼ýÀÚ8¹öÆ°
		JButton buttonNumber8 = new JButton("8");
		buttonNumber8.setForeground(new Color(0, 0, 0));
		buttonNumber8.setBackground(new Color(135, 206, 235));
		buttonNumber8.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Number("8");
			}
		});
		buttonNumber8.setBounds(124, 204, 80, 80);
		frame.getContentPane().add(buttonNumber8);
		
		
		
		
		JButton buttonNumber9 = new JButton("9");
		buttonNumber9.setForeground(new Color(0, 0, 0));
		buttonNumber9.setBackground(new Color(135, 206, 235));
		buttonNumber9.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Number("9");
			}
		});
		buttonNumber9.setBounds(226, 204, 80, 80);
		frame.getContentPane().add(buttonNumber9);
		
		
		
		 
		 
		/*  키보드를 위해서 있는건가???===============================================================
		//¼ýÀÚ9¹öÆ°
		JButton buttonNumber9 = new JButton("9");
		buttonNumber9.setForeground(new Color(0, 0, 0));
		buttonNumber9.setBackground(new Color(135, 206, 235));
		buttonNumber9.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).NumberActionListener("9");
			}
		});
		buttonNumber9.setBounds(226, 204, 80, 80);
		frame.getContentPane().add(buttonNumber9);
		//엔터면 아무것도 없고 숫자이면 입력한다.... 쌓인다,,, 
		
		frame.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(KeyEvent event)
			{
				char inputedKey = event.getKeyChar();
				int key = event.getKeyCode();

				if(inputedKey>='0' && inputedKey <='9') {
					String keyString = "";
					keyString+=inputedKey;
					((InputedNumberController)calculator).NumberActionListener(keyString);
				}


				else if(key == KeyEvent.VK_ENTER)
				{
					
				}
			}
				
		});
		
		*/
		
		
		
		
		
		
		//부호 란====================================================================
		
		//나누기곱하기
		JButton buttonDivi = new JButton("÷");
		buttonDivi.setForeground(new Color(0, 0, 0));
		buttonDivi.setBackground(new Color(135, 206, 235));
		buttonDivi.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Cal("÷");  //(InputedNumberController)calculator) 형식 모르겠음 
			}
		});
	
		buttonDivi.setBounds(320, 110, 80, 80);   
		frame.getContentPane().add(buttonDivi);
		
		
		//곱하기 
		JButton buttonMulti = new JButton("X");
		buttonMulti.setForeground(new Color(0, 0, 0));
		buttonMulti.setBackground(new Color(135, 206, 235));
		buttonMulti.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Cal("×");
			}
		});
	
		buttonMulti.setBounds(320, 203, 80, 80);    
		frame.getContentPane().add(buttonMulti); 
		
		
		//뺴기 
		JButton buttonMinus = new JButton("－");
		buttonMinus.setForeground(new Color(0, 0, 0));
		buttonMinus.setBackground(new Color(135, 206, 235));
		buttonMinus.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Cal("-");
			}
		});
	
		buttonMinus.setBounds(320, 315, 80, 80);    
		frame.getContentPane().add(buttonMinus); 
		
		
		//더하기 
		JButton buttonPlus = new JButton("＋");
		buttonPlus.setForeground(new Color(0, 0, 0));
		buttonPlus.setBackground(new Color(135, 206, 235));
		buttonPlus.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Cal("+");
			}
		});
	
		buttonPlus.setBounds(320, 427, 80, 80);    
		frame.getContentPane().add(buttonPlus); 
		
		
		
		//결과
		JButton buttonResult = new JButton("=");
		buttonResult.setForeground(new Color(0, 0, 0));
		buttonResult.setBackground(new Color(135, 206, 235));
		buttonResult.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Last("=");
			}
		});
	
		buttonResult.setBounds(320, 527, 80, 80);    
		frame.getContentPane().add(buttonResult); 
		
		
		//초기화
		JButton buttonClear = new JButton("C");
		buttonClear.setForeground(new Color(0, 0, 0));
		buttonClear.setBackground(new Color(135, 206, 235));
		buttonClear.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Clear("C");
			}
		});
		buttonClear.setBounds(19, 110, 80, 80);   
		frame.getContentPane().add(buttonClear);
		
		
	
		//하나 지우기
		JButton buttonBack= new JButton("←");
		 buttonBack.setForeground(new Color(0, 0, 0));
		 buttonBack.setBackground(new Color(135, 206, 235));
		 buttonBack.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController)calculator).Clear("←");
			}
		});
	
		 buttonBack.setBounds(226, 110, 80, 80);    
		frame.getContentPane().add(buttonBack); 
		
		
		
	}
}
