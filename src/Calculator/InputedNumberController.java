package Calculator;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class InputedNumberController extends CalculatorMain {
	// String Input=""; //숫자만 넣는다
	// String CopyInput="";// =계속눌러서 연산 할때를 대비함
	// String Cal=""; // 연산자만 넣는다.
	// String Check=""; //= 을 감지하기 위한놈
	// int Result=0;//소수점 감안해야함
	double a =.000;
	String Input = "";
	// 숫자만 넣는다
	String Read = "";
	// 첫번째 라벨 텍스트 표시를 위한 쌓는문
	double Result = 0;
	// 2번째 라벨 표시기
	String Buho;
	int Back = 0;

	int CH = 0;
	ArrayList<String> Numlist = new ArrayList<String>();
	ArrayList<String> Callist = new ArrayList<String>();
	int num1=1;
	int num2=2;
	//BigDecimal abc = new BigDecimal(num1);
	//BigDecimal bcd = new BigDecimal(num2);
	
	
	public void Number(String num) {
		
		//double c = abc.add(bcd).doubleValue();
		
		// 자리수 방지
		if (Input.length() > 15) {
			num = "";
		}

		// . 점을 2번찍는것을 알아서 방지 한
		if (num.equals("0")) {
			if (Input.equals("")) {
				answerRecord.setText("0");
				Input +="";
				Read += "";
			}

			// 0찍기 방지
			else if (Read.equals("0")) {
				answerRecord.setText("0");
				Input +="";
				Read += "";
			}
			// 0다음에 0안찍히는거 ~~~~~ 처음에 0이 있다면 아무것도 하지 않는다.
			else if (String.valueOf(Input.charAt(0)).equals("0")) {
			}
		}

		if (num.equals(".")) {
			if (Input.equals("")) {
				answerRecord.setText("0.");
				Input = "0.";
				Read += "0.";
			}

			// 0일때 0.유지하기
			else if (Input.equals("0")) {
				answerRecord.setText("0.");
				Input = "0.";
				Read += ".";
			}
			// 0. 일때 .유지하기
			else if (Input.equals("0.")) {
				answerRecord.setText("0.");
				Input = "0.";
				Read = Read.substring(0, Read.length() - 1);
				Read += ".";
			}
			// 숫자 받는곳에 마지막 기록이 .이면 아무것도 하지 않는다
			else if ((Input.substring(Input.length() - 1).equals(num))) {
			}
			// 정수인지 소수저인지 판별함
			else if (Double.parseDouble(Input) % 1 == 0) {
				Input += num;
				Read += num;
				answerRecord.setText(Input);
			} else if (Double.parseDouble(Input) % 1 != 0) {}
			// Read.substring(Read.length() - 1).equals(cal)
			// 증간에 점넣기 방지
		}

		else {
			// 0이 들어가져 있으면 다음 숫자들어가면 숫자를 덮어 씌움
			if (Read.equals("0")) {
				Input = num;
				Read = num;
				answerRecord.setText(Input);
			} else {
				Input += num;
				Read += num;
				answerRecord.setText(Input);
			}
		}
		//System.out.println(Input.length());
	}

	public void Cal(String cal) {

		//System.out.println(Read);

		// 처음에 아무것도 없이 연산자 누를때

		if (Read.equals("")) {
			Numlist.add("0");
			Callist.add(cal);
			if (cal.equals("+")) {
				Read = Read + 0 + "+";
				Result = Result + 0;
			} else if (cal.equals("-")) {
				Read = Read + 0 + "-";
				Result = Result - 0;
			}

			else if (cal.equals("×")) {
				//System.out.println(Read);
				Read = Read + 0 + "×";
			//	System.out.println(Read);
				Result = Result * 0;
			}

			else if (cal.equals("÷")) {
				Read = Read + 0 + "÷";
				//
				// Result=Result/0;
			}

			Numlist.add("0");
			Callist.add(cal);
			recordLabel.setText(Read);
			// answerRecord.setText(Integer.toString(Result));
			// answerRecord.setText(Double.toString(Result));
			if (Result == (long) Result)
				answerRecord.setText(String.format("%d", (long) Result));
			else
				answerRecord.setText(String.format("%s", Math.round(Result*1000000000000000.0)/1000000000000000.0));
				//answerRecord.setText(String.format("%s", Result));
			// System.out.println(cal);
		}

		// 중복 방지
		else if (Read.substring(Read.length() - 1).equals(cal)) {}
//=====================================================================================================================================
		else {
			// 연산자가 이미있는데 또 연산자가 들어왔을때 처리 출력부분 하나지우고 새거 넣고 리스트에 하나 지우고 새거 넣기
			if (Read.substring(Read.length() - 1).equals("+") || Read.substring(Read.length() - 1).equals("-")
					|| Read.substring(Read.length() - 1).equals("×") || Read.substring(Read.length() - 1).equals("÷")) {
				
				//System.out.println(Input);
				//System.out.println(Read);
				
				//이 부분은 Input을 초기화 하기 떄문에 소수점 길이가 존재 할시 아래 사항을 적용한다. 
				if(Input.length()>1) {
					if(Input.substring(Input.length() - 1).equals(".")) {
						Input = Input.substring(0, Input.length() - 1);
						Read = Read.substring(0, Read.length() - 1);
					}
				}
				Read = Read.substring(0, Read.length() - 1);
				Read += cal;
				recordLabel.setText(Read);

				Callist.remove(Callist.size() - 1);
				Callist.add(cal);
				// 리스트 마지막꺼 삭제
				// 새로운 리스트 추가
				
				
			} 
			else {
				// 2*============ 하다가 + 했을때 오류
				if (Input.equals("")) {
					Input = ((Numlist.get(Numlist.size() - 1)));
				}

				if(Input.substring(Input.length() - 1).equals(".")) {
					Input = Input.substring(0, Input.length() - 1);
					Read = Read.substring(0, Read.length() - 1);
				}
				
				Numlist.add(Input);
				Callist.add(cal);
				Input = "";

				// 연산자 들어올때 혹시 마지막이 =이면 최근 결과값을 보여주고 시작하자
				if (!(Read.substring(Read.length() - 1).equals("="))) {

					Result = 0;
					for (int Up = 0; Up < Numlist.size(); Up++) {

						if (Up == 0) {
							Result += Double.parseDouble(Numlist.get(0));
						}

						else {
							if (Callist.get(Up - 1).equals("+")) {
								Result = Result + Double.parseDouble(Numlist.get(Up));
							}
							if (Callist.get(Up - 1).equals("-")) {
								Result = Result - Double.parseDouble(Numlist.get(Up));
							}
							if (Callist.get(Up - 1).equals("×")) {
								Result = Result * Double.parseDouble(Numlist.get(Up));
							}
							if (Callist.get(Up - 1).equals("÷")) {
								Result = Result / Double.parseDouble(Numlist.get(Up));
							}
						}
					}
					// 중목으로 플러스나 마이너스 누르면
					Read = Read + cal;
					recordLabel.setText(Read);
					// answerRecord.setText(Integer.toString(Result));
					// answerRecord.setText(Double.toString(Result));
					if (Result == (long) Result)
						answerRecord.setText(String.format("%d", (long) Result));
					else
						answerRecord.setText(String.format("%s", Math.round(Result*1000000000000000.0)/1000000000000000.0));
						//answerRecord.setText(String.format("%s", Result));
				}

				if (Read.substring(Read.length() - 1).equals("=")) {
					Read = Double.toString(Result) + cal;

					// recordLabel.setText(Integer.toString(Result)+cal);
					recordLabel.setText(Read);
					// answerRecord.setText(Integer.toString(Result));
					// answerRecord.setText(Double.toString(Result));
					if (Result == (long) Result)
						answerRecord.setText(String.format("%d", (long) Result));
					else
						answerRecord.setText(String.format("%s", Math.round(Result*1000000000000000.0)/1000000000000000.0));
					//	answerRecord.setText(String.format("%s", Result));
					// 축적이 될까????

				}
			}
		}
		//System.out.println(Read);
	}

	public void Clear(String clear) {
		if (clear.equals("C")) {
			Input = "";
			Result = 0;
			clear = "";
			Read = "";

			Numlist.clear();
			Callist.clear();
			recordLabel.setText(Read);
			answerRecord.setText("");
		}

		if (clear.equals("←")) {
			String Delete = answerRecord.getText();

			if (Delete.equals(Result)) {
				char last = Read.charAt(Read.length() - 1);
				String Check = Character.toString(last);

				if (Check.equals("+") | Check.equals("-") | Check.equals("×") | Check.equals("÷")) {
				}

				else {
					// Read=Integer.toString(Result);
					Read = Double.toString(Result);

					recordLabel.setText("");
					// answerRecord.setText(Integer.toString(Result));
					
					answerRecord.setText(Double.toString(Result));

					Result = 0;
					Read = "";
				}
			}

			else {
				if (Delete.equals("")) {
					// 아무것도 없으면 아무것도 안한다
				}
				// 아래에 만든이유는 125-25에서 중간에 5를 지우면 안정적으로 127이 계산된다
				else if (Delete.equals(Input)) {
					Input = Input.substring(0, Delete.length() - 1);
					Read = Read.substring(0, Read.length() - 1);
					answerRecord.setText(Input);
				}
			}
		}
	}

	public void Last(String Check) {
		//int b=Double.compare(Result,a);
		
		
		//System.out.println(b);
		
		if (Read.equals("")) {

		}

		// 만일 위에 텍스트 출력하는 부분에 마지막 글자가 =이면 중복되게 하고 아니면 마지막 연산자 리스트에 애를 가져와서 보고 계산한다.

		else {
			
			// c축적
			// ===============================================================================================
			// 5+5=
			if (Read.substring(Read.length() - 1).equals("=")) {
				// Read=Read.substring(0, Read.length()-1 );//마지막 문자제거
				// Read=Read+Check;
				// System.out.println(Result);
				// System.out.println(Input);

				if (Callist.get(Callist.size() - 1).equals("+")) {
					// ======================================================================================
					Read = Result + "+" + Numlist.get(Numlist.size() - 1) + Check;
					// Read=Result+"+"+Numlist.get(Numlist.size()-1)+Check;
					Result = Result + Double.parseDouble(Numlist.get(Numlist.size() - 1));
				}

				if (Callist.get(Callist.size() - 1).equals("-")) {
					Read = Result + "-" + Numlist.get(Numlist.size() - 1) + Check;
					// Read=Result+"-"+Numlist.get(Numlist.size()-1)+Check;
					Result = Result - Double.parseDouble(Numlist.get(Numlist.size() - 1));
				}

				if (Callist.get(Callist.size() - 1).equals("×")) {
					Result=Math.round(Result*1000000000000000.0)/1000000000000000.0;
					Read = Result + "×" + Numlist.get(Numlist.size() - 1) + Check;
					// Read=Result+"×"+Numlist.get(Numlist.size()-1)+Check;
					Result = Result * Double.parseDouble(Numlist.get(Numlist.size() - 1));
				}

				if (Callist.get(Callist.size() - 1).equals("÷")) {
					Result=Math.round(Result*1000000000000000.0)/1000000000000000.0;
					if (Result == 0 && Numlist.get(Numlist.size() - 1).equals(0)) {
						Reset();
						Back = 1;
					}

					if (Back == 0) {
						Read = Result + "÷" + Numlist.get(Numlist.size() - 1) + Check;
						Result = Result / Double.parseDouble(Numlist.get(Numlist.size() - 1));
					}
				}

				if (Back == 0) {
					Numlist.add(Numlist.get(Numlist.size() - 1));
					//Numlist.add(Numlist.get(Numlist.size() - 1));
					recordLabel.setText(Read);

					// answerRecord.setText(Double.toString(Result));
					if (Result == (long) Result)
						answerRecord.setText(String.format("%d", (long) Result));
					else
						answerRecord.setText(String.format("%s", Math.round(Result*1000000000000000.0)/1000000000000000.0));
						//answerRecord.setText(String.format("%s", Result));
				}
			}

			else {

				// =========================================================================================================
				if (Read.substring(Read.length() - 1).equals("+") || Read.substring(Read.length() - 1).equals("-")
						|| Read.substring(Read.length() - 1).equals("×")
						|| Read.substring(Read.length() - 1).equals("÷")) {

					if (Read.substring(Read.length() - 1).equals("+")) {
						Read = Read + Result + Check;
						Numlist.add(Double.toString(Result));
						Result = Result + Result;
					}
					if (Read.substring(Read.length() - 1).equals("-")) {
						Read = Read + Result + Check;
						Numlist.add(Double.toString(Result));
						Result = Result - Result;
					}
					if (Read.substring(Read.length() - 1).equals("×")) {
						Read = Read + Result + Check;
						Numlist.add(Double.toString(Result));
						Result = Result * Result;
					}
					if (Read.substring(Read.length() - 1).equals("÷")) {
						if (Numlist.get(Numlist.size() - 1).equals(0)) {
							Reset();
							Back = 1;
						}
						if (Back == 0) {
							Read = Read + Result + Check;
							Numlist.add(Double.toString(Result));
							Result = Result / Result;
						}
					}

					if (Back == 0) {
						// 2+5+6-8X
						Numlist.add(Numlist.get(Numlist.size() - 1));
						Callist.add(Callist.get(Callist.size() - 1));
						recordLabel.setText(Read);
						// answerRecord.setText(Integer.toString(Result));

						if (Result == (long) Result)
							answerRecord.setText(String.format("%d", (long) Result));
						else
					
							//answerRecord.setText(String.format("%s", Math.round(Result*1000000000000000.0)/1000000000000000.0));
							
							answerRecord.setText(String.format("%s", Math.round(Result*1000000000000000.0)/1000000000000000.0));
						
					}
				}

				// Read 전에가 숫자라면 //10+2
				
				
				else {
					if(Read.substring(Read.length() - 1).equals(".")) {
						Read=Read.substring(0, Read.length() - 1);
					}
					if(Input.substring(Input.length() - 1).equals(".")) {
						Input=Input.substring(0, Input.length() - 1);
					}
					if (Callist.get(Callist.size() - 1).equals("+")) {
						// Result = Result + Integer.parseInt(Input);
						Result = Result + Double.parseDouble(Input);
					}
					if (Callist.get(Callist.size() - 1).equals("-")) {
						Result = Result - Double.parseDouble(Input);
					}
					if (Callist.get(Callist.size() - 1).equals("×")) {
						Result = Result * Double.parseDouble(Input);
					}
					if (Callist.get(Callist.size() - 1).equals("÷")) {
						if (Numlist.get(Numlist.size() - 1).equals(0)) {
							Reset();
							Back = 1;
						}
						if (Back == 0) {
							Result = Result / Double.parseDouble(Input);
						}
					}
					if (Back == 0) {
						// 2*5=10 = 10*5= 50
						if(Input.substring(Input.length() - 1).equals(".")) {
							Input=Input.substring(0, Read.length() - 1);
						}
						Numlist.add(Input);
						Read = Read + Check;
						recordLabel.setText(Read);
						// answerRecord.setText(Integer.toString(Result));

						if (Result == (long) Result)
							answerRecord.setText(String.format("%d", (long) Result));
						else {
							answerRecord.setText(String.format("%s", Math.round(Result*1000000000000000.0)/1000000000000000.0));
							//answerRecord.setText(String.format("%s", Result));
							//System.out.println(Math.round(Result*1000000000)/1000000000.0);
							}
						}
				}

			}
		}
	//	System.out.println(Input);
	//	System.out.println(Read);
	}

	public void Reset() {
		Input = "";
		Result = 0;
		// clear="";
		Read = "";

		Numlist.clear();
		Callist.clear();
		recordLabel.setText("0÷");
		answerRecord.setText("정의 할수 없는 수입니다");
	}
}
